guess_host_triple: Guess which Rust-supported platform is running the current code
==================================================================================

Repository: <https://gitlab.com/Screwtapello/guess_host_triple>

Documentation: <https://docs.rs/guess_host_triple>
